#!/bin/bash
set -x

mkdir -p target

docker run --rm --name mk2pdf -v $PWD:/data mk2pdf jinja2 /data/src/debian-trademark.md.tpl /data/infos.yaml -o /data/target/result.md
docker run --rm --name mk2pdf -v $PWD:/data mk2pdf pandoc /data/target/result.md -o /data/target/result.html
docker run --rm --name mk2pdf -v $PWD:/data mk2pdf wkhtmltopdf --enable-local-file-access /data/target/result.html /data/target/result.pdf
