FROM debian:11-slim

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y \
    pandoc python3-pip wkhtmltopdf && \
    apt-get autoremove -y && \
    apt-get clean -y
RUN pip install jinja2-cli[yaml]
