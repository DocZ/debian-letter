# Debian mark confirmation

I, the undersigned **{{ name }}** resident in {{ residency }}, declare the following:

I have worked as **{{ main_title.job }}** since {{ main_title.period }} and I am in
charge of {{ main_title.charge}}.
{% if titles %}
Before that I worked for:
  {% for item in titles %}
- **{{ item.period }}** for **{{ item.employer }}** as _{{item.job }}_

  {%- endfor %}
{% endif %}
Thanks to my activity and the long professional experience I know the most
important names in the software and hardware sectors, including the mark
**Debian**, with which I have been familiar for many years, and which can
certainly be considered to be a well-known mark in Switzerland.

As a result of my job, I am aware of the fact that **Debian** is **widely
adopted** in prominent products and services: {{main_argument }}
{% if arguments -%} As well as, other types of products such as:
  {% for item in arguments -%}
  {{ item }},
  {%- endfor %}
{% endif %}
both in shops located
through the {{ country }} territory and on-line.

To the best of my knowledge, I can confirm that the mark **Debian** is
unequivocally associated by {{ country }} consumers with the project that bears the
same name.

Faithfully,

![](../{{ signature }})

{{ date }},

{{ name }}
