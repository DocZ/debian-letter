# debian-letter

## Getting started

1. Clone repository

```bash
git clone --depth=1 https://gitlab.com/DocZ/debian-letter.git
```

2. Prepare docker image

```bash
cd debian-letter
docker build -t mk2pdf .
```

3. Create an signature image and save it in the same dir

4. Copy and edit the `infos-sample.yaml` file, don't forget the signature file.

```bash
cp info-sample.yaml infos.yaml
# edit infos.yaml file
```

5. Generate PDF letter

```bash
./generate.sh

### Output should looks like:
#+ mkdir -p target
#+ docker run --rm --name mk2pdf -v YOUR_PWD_PATH:/data mk2pdf jinja2 /data/src/debian-trademark.md.tpl /data/infos.yaml -o /data/target/result.md
#+ docker run --rm --name mk2pdf -v YOUR_PWD_PATH:/data mk2pdf pandoc /data/target/result.md -o /data/target/result.html
#+ docker run --rm --name mk2pdf -v YOUR_PWD_PATH:/data mk2pdf wkhtmltopdf --enable-local-file-access /data/target/result.html /data/target/result.pdf
#QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'
#Loading page (1/2)
#Printing pages (2/2)
#Done
```
